from Grid.grid_utils import gridChallenge

import unittest

class GridTest(unittest.TestCase):
    def test_character_caseone(self):
        ans = 'YES'
        grid = ['ebacd', 'fghij', 'olmkn', 'trpqs', 'xywuv']
        ac = "incorrect"
        self.assertEqual(gridChallenge(grid), ans,ac)

    def test_character_casetwo(self):
        ans = 'NO'
        grid = ['vbznfwg', 'eacklwk', 'bmarzvp', 'rwgnjqd', 'xqddtln', 'wuxtduk',  'rzmfcik']
        ac = "incorrect"
        self.assertEqual(gridChallenge(grid), ans,ac)
    
    