from CatMouse.catmouse_utils import catAndMouse

import unittest

class FunnyTest(unittest.TestCase):
    def test_cat_mouse_caseone(self):
        catfirst = 1
        catsecound = 2
        mouse = 3
        ans = 'Cat B'
        self.assertEqual(catAndMouse(catfirst,catsecound,mouse),ans)

    def test_cat_mouse_casetwo(self):
        catfirst = 2
        catsecound = 1
        mouse = 3
        ans = 'Cat A'
        self.assertEqual(catAndMouse(catfirst,catsecound,mouse),ans)

    def test_cat_mouse_casethree(self):
        catfirst = 2
        catsecound = 2
        mouse = 3
        ans = 'Mouse C'
        self.assertEqual(catAndMouse(catfirst,catsecound,mouse),ans)