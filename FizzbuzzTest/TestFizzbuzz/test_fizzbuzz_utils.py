from FizzBuzz.fizzbuzz_utils import FizzBuzz

import unittest

class FizzBuzzTest(unittest.TestCase):
    def test_fizzbuzz_caseone(self):
        in_fun = 4
        ans_fun = 'NoFizzBuzz'
        self.assertEqual(FizzBuzz(in_fun),ans_fun)

    def test_fizzbuzz_casetwo(self):
        in_fun = 6
        ans_fun = 'Fizz'
        self.assertEqual(FizzBuzz(in_fun),ans_fun)

    def test_fizzbuzz_casethree(self):
        in_fun = 15
        ans_fun = 'FizzBuzz'
        self.assertEqual(FizzBuzz(in_fun),ans_fun)

    def test_fizzbuzz_casefour(self):
        in_fun = 5
        ans_fun = 'Buzz'
        self.assertEqual(FizzBuzz(in_fun),ans_fun)