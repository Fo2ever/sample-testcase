from AlternatingChr.alternating_utils import alternatingCharacters

import unittest

class AlternatingTest(unittest.TestCase):
    def test_character_caseone(self):
        ans_fun = 4
        ac = "incorrect"
        self.assertEqual(alternatingCharacters('BBBBB'), ans_fun,ac)
    
    def test_character_casetwo(self):
        ans_fun = 0
        ac = "incorrect"
        self.assertEqual(alternatingCharacters('ABABABAB'), ans_fun,ac)