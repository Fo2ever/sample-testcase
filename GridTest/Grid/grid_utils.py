def gridChallenge(grid):
    
    sort = []
    for i in range(len(grid)):
        sort.append(sorted(grid[i]))
        
    for l in range(len(sort[0])):
        sorts = []
        for j in range(len(sort)):
            sorts.append(sort[j][l])
        if sorts != sorted(sorts):
            return 'NO'
        
    return 'YES'