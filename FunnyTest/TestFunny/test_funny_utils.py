from FunnyString.funny_utils import funnyString

import unittest

class FunnyTest(unittest.TestCase):
    def test_character_caseone_funny(self):
        ans_fun = 'Funny'
        ac = "incorrect"
        self.assertEqual(funnyString('acxz'), ans_fun,ac)
    
    def test_character_casetwo_funny(self):
        ans_fun = 'Not Funny'
        ac = "incorrect"
        self.assertEqual(funnyString('bcxz'), ans_fun,ac)