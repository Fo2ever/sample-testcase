from TwoCharacters.twocharacter_utils import alternate

import unittest

class TwoCaractersTest(unittest.TestCase):
    def test_character_caseone(self):
        in_fun = 'beabeefeab'
        ans_fun = 5
        self.assertEqual(alternate(in_fun),ans_fun)

    def test_character_casetwo(self):
        in_fun = 'aaaaa'
        ans_fun = 0
        self.assertEqual(alternate(in_fun),ans_fun)