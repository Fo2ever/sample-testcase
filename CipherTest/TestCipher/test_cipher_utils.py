from CaesarCipher.cipher_utils import caesarCipher

import unittest

class CipherTest(unittest.TestCase):
    def test_character_caseone(self):
        in_fun = '159357lcfd'
        k = 98
        ans_fun = '159357fwzx'
        self.assertEqual(caesarCipher(in_fun,k),ans_fun)

    def test_character_casetwo(self):
        in_fun = 'middle-Outz'
        k = 2
        ans_fun = 'okffng-Qwvb'
        self.assertEqual(caesarCipher(in_fun,k),ans_fun)