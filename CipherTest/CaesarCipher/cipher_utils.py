import string

def caesarCipher(s, k):
    k = k % 26
    wordlo = string.ascii_lowercase
    wordup = string.ascii_uppercase
    textlo = wordlo[k:] + wordlo[:k]
    textup = wordup[k:] + wordup[:k]
    result = ''
    for i in s:
        if i in wordlo:
            ac = wordlo.index(i)
            result += textlo[ac]
        elif i in wordup:
            ca = wordup.index(i)
            result += textup[ca]
        elif i not in textlo and textup:
            result += i
            
    return result 