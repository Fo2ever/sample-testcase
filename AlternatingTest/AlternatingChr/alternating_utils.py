def alternatingCharacters(alternation):
    result = 0
    for i in range(len(alternation)-1):
        if alternation[i] == alternation[i+1]:
            result += 1
    return result
    